# QR Code Generator for OpenBis - DeMoraes/Mescher Lab, ETH Zurich (v.20240819)

This document outlines the process of generating QR codes with metadata for printing on cryogenic Labtag labels (24mm x 13mm) using Python, Excel, and Adobe Illustrator. The labels are from [Labtag](https://www.labtag.com/shop/product/cryo-laser-labels-0-945-x-0-512-24mm-x-13mm-a4cl-12/).

## Directory Structure

Ensure the following directory structure is present in the working directory, as the scripts depend on it:
- ./data/raw
- ./output
- ./src
- ./layouts

- Place the following scripts in the `./src` folder.
	- *./src/qrcode-gen_current.py*
	- *./src/excel_to_pdf_converter.py*

- Place the following file in the `./layouts` folder.
	- *./layouts/Layout_with_gutter_A4.ai*
	
## Requirements

Before starting, ensure you have the following software installed:

- [Select Menu Plug-in](https://rj-graffix.com/downloads/plugins/)
- Python 3.x
- Excel
- Adobe Illustrator and Acrobat

## Workflow

### 1. Prepare the PermID CSV

Start with the PermID CSV that you have previously downloaded from OpenBIS. A good example is:

`./data/raw/permID_marion.csv`

- Add a second column with human-readable metadata about the sample.
- Ensure every line has a maximum of 16 characters.
- Write a maximum of 5 lines with:
  - Project name
  - Unique OpenBIS code for the project
  - Sample ID
  - Phase (if applicable, e.g., in metabolomics)
  - Date

Example:

| ID                     | Details                                                      |
|------------------------|--------------------------------------------------------------|
| 20230706181655284-7091 | PregStudy<br>mrisse_2023_01<br>x1_empty_control<br>2022-12-16 |
| 20230706181655284-7092 | PregStudy<br>mrisse_2023_01<br>x2_Arm_control<br>2022-12-16  |
| 20230706181655284-7093 | PregStudy<br>mrisse_2023_01<br>x3_Foot_control<br>2022-12-16 |
| 20230706181655284-7094 | PregStudy<br>mrisse_2023_01<br>C1.B_Arm_sample<br>2022-12-16 |
| 20230706181655284-7095 | PregStudy<br>mrisse_2023_01<br>C1.B_Foot_sample<br>2022-12-16 |

### 2. Run Python Script

Run the Python script `qrcode-gen_current.py`: 

```bash
	python qrcode-gen_current.py
```

This script performs the following tasks:
	- Prompts the user to select a CSV file from the 'data/raw' directory
	- Generates an `output.xlsx` file with the QR codes and metadata in the `./output` directory. 
	

### 3. Adjust Page Margins and Cell Dimensions
Format the Excel file to have 20 x 8 sheets.
1. Change page margins:
   - Go to **View > Page Layout**.
   - Go to **Home > Wrap Text**.
   - Change size to 5 pt. or until the text fits the cells.
   - Align Bottom and Right.

![Description](images/image1.png)

2. Set custom margins:
   - Go to **Page Layout > Margins > Custom Margins**:
     - Top, Left, and Right: `8`
     - Bottom: `7`
     - Header and Footer: `0`
     - Center on Page: Horizontally

![Description](images/image2.png)

3. Set row height and column width:
   - For row height: Press `Alt + H + O + H`, enter `14.6mm` (Excel may adjust this slightly).
   - For column width: Select the column(s), press `Alt + H + O + W`, enter `24.08mm`.

![Description](images/image3.png)
![Description](images/image4.png)

### 4. Save Excel as PDF

You have two options to save the Excel file as a PDF:

1. Save directly as a PDF from Excel (not recommended).
2. Run the Python script `excel_to_pdf_converter.py`:

```bash
	python excel_to_pdf_converter.py
```

The second option generates a more structured and organized PDF, which is easier to handle in Illustrator. The output file will be saved in the `./output` folder.

### 5. Edit PDF in Adobe Illustrator

1. Open the saved `output.pdf` in Illustrator.
2. Also open the layout for the labels:

   `./layouts/Layout_with_gutter_A4.ai`

3. Copy the "layout" layer.
4. Paste layout as new layer in the output document containing the QR codes
   - Go to the output document
   - Create a new layer.
   - **Edit > Paste on All Artboards** (`Alt + Ctrl + Shift + V`).
   - Now you should have the layout in all the pages of your QR codes.

Align the QR codes and text within the cells of the layout.

### 6. Remove Paths and Clipping Masks

1. Select Objects **Select > Object > Art by Name** (after downloading the plug-in).
2. Copy the name `<Clipping Path>` and search all objects with that name, then delete them.

![Description](images/image5.png)

### 7. Move QR Codes to a New Layer

1. Ungroup layers twice:
   - Select all objects in the layer by clicking on the layer.
   - **Ungroup** (`Ctrl + Shift + G`). Repeat.
2. Select QR codes**Select > Object > Art by Name**.
3. Copy the name `<Image>` and search all objects with that name.
4. Click on Add new layer.
5. Move QR codes to that layer with **Object > Arrange > Send to Current Layer**.
6. Rename the new layer to `QR`.
7. Rename the remaining layer to `text`.
8. Ensure that `QR` contains only the QR images and `text` only the text.

### 8. Align QR Codes

1. Align to the top left corner.
2. Align using the move option:
   - Select the QR layer (all QR codes).
   - **Move** (`Ctrl + Shift + M`):
     - Horizontal: `0.1cm`
     - Vertical: `0.3cm`

If some columns are not aligned correctly, manually align each QR code to the top left corner of the grid, then move `0.1cm` to the right and `0.1cm` down. 
This ensures that the QR code is not cut off during printing.
Depending on the file, you might need to change the values and do it for each column, row independently.

### 9. Align Text

1. Align to the bottom right corner.
2. Select the text layer:
   - **Move** (`Ctrl + Shift + M`):
     - Horizontal: `0cm`
     - Vertical: `0.1cm`

Depending on the file, you might need to change the values and do it for each column, row independently.

### 10. Adjust Font Size

Some lines of text might be below the QR code and not visible. Adjust the font size accordingly.

### 11. Save as PDF

Save two versions of the PDF:
- One with the grid visible for test printing
- One with the grid hidden for final printing

### 12. Print PDF

1. Open the PDF and print. Ensure that **Page Sizing** is set to "Actual Size."
2. First, print the version with the grid and confirm that it aligns with the grid of the actual labels.
3. If everything aligns correctly, print the version without the grid.
4. Load the paper into tray 4.

---

## Additional Notes - Requirements

- The scripts were tested on Windows 10.
- Python version 3.11.5 was used for testing.

This guide helps you generate and print QR codes on Labtag labels while ensuring proper alignment and formatting. If you encounter any issues, please contact Hannier


