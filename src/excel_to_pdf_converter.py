"""
This Python script uses the `xlwings` library to convert an Excel workbook (.xlsx) to a PDF while preserving 
all original formatting, cell sizes, and embedded images. It searches for Excel files in the './output' directory 
relative to the script's location and prompts the user to select one for conversion. The PDF is saved in the same 
directory with the same name as the Excel file but with a .pdf extension.

### Key Considerations:
- Requires Microsoft Excel to be installed, as the script controls Excel via `xlwings`.
- The Excel window remains hidden during execution (`visible=False`), but you can set it to `True` for debugging.
- The script is independent of absolute paths, making it suitable for version control.

### Requirements:
- Install `xlwings`: `pip install xlwings`
"""

import os
import xlwings as xw

# Get the current script directory
script_dir = os.path.dirname(os.path.abspath(__file__))

# Define the output folder relative to the script location
output_folder = os.path.join(script_dir, '..', 'output')

# Search for Excel files in the output folder
excel_files = [f for f in os.listdir(output_folder) if f.endswith('.xlsx')]

# Prompt the user to select an Excel file if there are any
if not excel_files:
    print("No Excel files found in the output folder.")
else:
    print("Select an Excel file to convert to PDF:")
    for idx, file_name in enumerate(excel_files):
        print(f"{idx + 1}. {file_name}")

    # Get user input
    selected_index = int(input("Enter the number of the file: ")) - 1
    input_file = os.path.join(output_folder, excel_files[selected_index])

    # Define the output PDF file path
    output_file = os.path.join(output_folder, os.path.splitext(excel_files[selected_index])[0] + '.pdf')

    # Open the Excel application
    app = xw.App(visible=False)  # Set visible=True if you want to see the Excel window
    wb = app.books.open(input_file)

    # Export each sheet to a PDF
    wb.to_pdf(output_file)

    # Close the workbook and the app
    wb.close()
    app.quit()

    print(f"PDF generated successfully at {output_file}")
