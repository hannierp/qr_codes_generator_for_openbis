"""
QR Code Generator and Excel Exporter

This script performs the following tasks:
1. Prompts the user to select a CSV file from the 'data/raw' directory.
   - The CSV file should have two columns: the first column for permanent-ID retrieved from OpenBis (used for QR codes) 
   and the second column for associated sample information.

2. For each row in the selected CSV file:
   - Generates a QR code image based on the perm_ID.
   - Saves the QR code image in the 'codes' directory.

3. Creates an Excel file with the following characteristics:
   - The Excel file is saved in the 'output' directory.
   - Each QR code image is inserted into the Excel file.
   - The text from the CSV file is placed next to the QR code image in the corresponding cell.

The script ensures the necessary directories ('codes' and 'output') are created if they do not exist and is designed to work regardless of the working directory, 
assuming the relative folder structure remains intact.

Dependencies:
- `qrcode`: For generating QR code images.
- `openpyxl`: For creating and manipulating Excel files.
- `Pillow`: For handling image operations.

Usage:
1. Place your input CSV files in the 'data/raw' directory.
2. Run the script from the 'src' directory or any location where the relative paths are maintained.
3. Follow the prompts to select the CSV file.
4. The output Excel file will be saved in the 'output' directory with the name of the input file appended with '_output.xlsx'.
"""

import csv
import os
import qrcode
from openpyxl import Workbook
from openpyxl.drawing.image import Image
from openpyxl.styles import Alignment, Font
from openpyxl.utils import get_column_letter
from PIL import Image as PILImage

# Define directories using relative paths
base_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
data_dir = os.path.join(base_dir, 'data', 'raw')
output_dir = os.path.join(base_dir, 'output')
codes_dir = os.path.join(base_dir, 'codes')

# Create 'codes' directory if it doesn't exist
if not os.path.exists(codes_dir):
    os.makedirs(codes_dir)

# Create 'output' directory if it doesn't exist
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Prompt user to select the input file from the data/raw folder
print("Available CSV files in 'data/raw':")
csv_files = [f for f in os.listdir(data_dir) if f.endswith('.csv')]
if not csv_files:
    print("No CSV files found in 'data/raw'. Exiting.")
    exit()

for idx, file in enumerate(csv_files, start=1):
    print(f"{idx}. {file}")

file_index = int(input("Select the CSV file number: ")) - 1
input_file = csv_files[file_index]

# Full path to the input file
input_file_path = os.path.join(data_dir, input_file)

# Define the output Excel file based on the input file name
output_file_name = os.path.splitext(input_file)[0] + '_output.xlsx'
output_excel_path = os.path.join(output_dir, output_file_name)

# Open and read the selected CSV file
with open(input_file_path) as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')

    # Create a list to store the data for each row in Excel
    excel_data = []

    for line in csvReader:
        # Skip empty lines
        if not line:
            continue

        # Check if enough values are present
        if len(line) != 2:
            print("Invalid line:", line)
            continue

        # Destructure the line array into variables
        name = line[0]
        url = line[0]
        text = line[1]
        filename = f'{name}.png'
        file_path = os.path.join(codes_dir, filename)

        # Generate the QR code PNG using the URL
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=0,
        )
        qr.add_data(url)
        qr.make(fit=True)

        qr_img = qr.make_image(fill_color="black", back_color="white")
        qr_img.save(file_path)

        # Append data for the current row
        excel_data.append([name, text])

# Create a new Excel workbook
workbook = Workbook()
sheet = workbook.active

# Set column widths and row heights
for col in range(1, 9):
    sheet.column_dimensions[get_column_letter(col)].width = 24
for row in range(1, 21):
    sheet.row_dimensions[row].height = 42

# Insert the QR code images and text in the cells
for idx, row in enumerate(excel_data, start=1):
    name = row[0]
    text = row[1]
    qr_filename = f'{name}.png'
    qr_file_path = os.path.join(codes_dir, qr_filename)

    img = Image(qr_file_path)
    img.width = img.height = 35  # 72 pixels = 1 inch = 2.54 cm

    col_index = (idx - 1) // 20 + 1  # Calculate the column index
    row_index = (idx - 1) % 20 + 1  # Calculate the row index

    img.anchor = f'{get_column_letter(col_index)}{row_index}'
    sheet.add_image(img)

    cell_text = sheet.cell(row=row_index, column=col_index)
    cell_text.alignment = Alignment(horizontal="right", vertical="top")
    cell_text.font = Font(size=6)
    cell_text.value = text

# Save the Excel file in the output directory
workbook.save(output_excel_path)

print(f'QR codes added to {output_excel_path} successfully.')
